# Demoapps

Sample Demo apps Chart

## Installing the Chart

Add Helm Chart repository.

```
helm repo add demo-apps-helm-charts https://gitlab.com/yono1/helm-charts/docs
```

Install the chart.

```
helm install iotapps-demo/iotapps-demo
```