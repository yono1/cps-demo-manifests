#!/bin/bash

oc secrets link klusterlet open-cluster-management-image-pull-credentials --for=pull -n open-cluster-management-agent 
oc secrets link klusterlet-work-sa open-cluster-management-image-pull-credentials --for=pull -n open-cluster-management-agent
oc secrets link application-manager open-cluster-management-image-pull-credentials --for=pull -n open-cluster-management-agent-addon
oc secrets link cert-policy-controller-sa open-cluster-management-image-pull-credentials --for=pull -n open-cluster-management-agent-addon
oc secrets link cluster-proxy open-cluster-management-image-pull-credentials --for=pull -n open-cluster-management-agent-addon
oc secrets link config-policy-controller-sa open-cluster-management-image-pull-credentials --for=pull -n open-cluster-management-agent-addon
oc secrets link governance-policy-framework-sa open-cluster-management-image-pull-credentials --for=pull -n open-cluster-management-agent-addon
oc secrets link klusterlet-addon-search open-cluster-management-image-pull-credentials --for=pull -n open-cluster-management-agent-addon
oc secrets link klusterlet-addon-workmgr open-cluster-management-image-pull-credentials --for=pull -n open-cluster-management-agent-addon
oc secrets link klusterlet-addon-workmgr-log open-cluster-management-image-pull-credentials --for=pull -n open-cluster-management-agent-addon
oc secrets link managed-serviceaccount open-cluster-management-image-pull-credentials --for=pull -n open-cluster-management-agent-addon